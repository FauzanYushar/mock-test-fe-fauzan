import { Route, Routes, useNavigate } from "react-router";
import Login from "./pages/Login";
import ToDoList from "./pages/ToDoList";
import Register from "./pages/Register";
import { useEffect } from "react";

function App() {
  const pathname = window.location.pathname;
  const navigate = useNavigate();
  useEffect(() => {
    if (pathname == "/") {
      navigate("/login");
    }
  }, []);

  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/todolist/:id" element={<ToDoList />} />
      <Route path="/register" element={<Register />} />
    </Routes>
  );
}

export default App;
