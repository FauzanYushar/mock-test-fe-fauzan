import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../redux/auth/reducer";
import toDoReducer from "../redux/toDoList/reducer";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    todo: toDoReducer,
  },
});
