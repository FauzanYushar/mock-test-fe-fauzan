import React, { useEffect, useState } from "react";
import PinInput from "react-pin-input";
import { useNavigate } from "react-router";
import "../styles/login.css";
import { Button } from "react-bootstrap";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useDispatch, useSelector } from "react-redux";
import { postLogin, data as dataLogin, reset } from "../redux/auth/reducer";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { data, status_login } = useSelector(dataLogin);

  const handlePinInput = (value) => {
    const payload = { pin: Number(value) };
    dispatch(postLogin(payload));
  };

  const handleRegisterClick = () => {
    navigate("/register");
  };

  useEffect(() => {
    if (status_login == "succeeded") {
      dispatch(reset());
      navigate(`/todolist/${data.id}`);
    } else if (status_login == "failed") {
      dispatch(reset());
      window.location.reload(false);
    }
  });

  return (
    <div
      className="pinBox"
      style={{
        margin: "0px auto",
        width: "50%",
        height: "100vh",
        textAlign: "center",
        paddingTop: "40vh",
        boxSizing: "border-box",
      }}
    >
      <h1 style={{ margin: "0px" }}>Masukkan PIN Code</h1>
      <PinInput
        length={4}
        initialValue=""
        secret
        secretDelay={100}
        type="numeric"
        inputMode="number"
        inputStyle={{ borderColor: "black" }}
        inputFocusStyle={{ borderColor: "blue" }}
        onComplete={handlePinInput}
      />
      <div className="registerBox">
        <p>You don't have account?</p>
        <Button onClick={handleRegisterClick} variant="link">
          Register
        </Button>
      </div>
    </div>
  );
}

export default Login;
