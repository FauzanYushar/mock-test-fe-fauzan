import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import PinInput from "react-pin-input";
import "../styles/register.css";
import { useDispatch, useSelector } from "react-redux";
import {
  data as dataRegister,
  postRegister,
  reset,
} from "../redux/auth/reducer";
import { useNavigate } from "react-router";

const Register = () => {
  const [pin, setPin] = useState("");
  const [username, setUsername] = useState("");
  const dispatch = useDispatch();
  const { status_register } = useSelector(dataRegister);
  const navigate = useNavigate();
  const handleClick = (e) => {
    e.preventDefault();
    const payload = { username: username, pin: pin };
    dispatch(postRegister(payload));
  };

  useEffect(() => {
    if (status_register == "succeeded") {
      dispatch(reset());
      navigate(`/login`);
    } else if (status_register == "failed") {
      dispatch(reset());
      window.location.reload(false);
    }
  });

  return (
    <div>
      <div className="registerFormBox">
        <h4>REGISTER FORM</h4>
        <div
          style={{ border: "1px solid", padding: "5px", borderRadius: "10px" }}
        >
          <Form>
            <Form.Group className="mb-3" controlId="formBasicUsername">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="username"
                placeholder="Enter username"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>PIN</Form.Label>
              <PinInput
                length={4}
                initialValue=""
                secret
                secretDelay={100}
                type="numeric"
                inputMode="number"
                inputStyle={{ borderColor: "black" }}
                inputFocusStyle={{ borderColor: "blue" }}
                onComplete={(value) => setPin(value)}
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              disabled={!username || pin.length < 4}
              onClick={handleClick}
            >
              Submit
            </Button>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Register;
