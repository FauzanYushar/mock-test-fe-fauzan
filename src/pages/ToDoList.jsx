import { Button, Form, InputGroup } from "react-bootstrap";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useDispatch, useSelector } from "react-redux";
import {
  addToDoList,
  showToDoList,
  data as dataToDo,
  reset,
} from "../redux/toDoList/reducer";
import { logout } from "../redux/auth/reducer";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";

function ToDoList() {
  const dispatch = useDispatch();
  const { data, status_add } = useSelector(dataToDo);
  const [addToDo, setAddToDo] = useState("");
  const { id } = useParams();
  const navigate = useNavigate();

  const handleAdd = () => {
    const payload = { todo: addToDo, id: id };
    dispatch(addToDoList(payload));
  };

  const handleLogout = () => {
    dispatch(logout());
    navigate("/login");
  };

  useEffect(() => {
    if (status_add == "succeeded") {
      dispatch(showToDoList(id));
      dispatch(reset());
    }
  });

  useEffect(() => {
    dispatch(showToDoList(id));
  }, []);

  return (
    <div>
      <div
        className="inputToDoList"
        style={{
          backgroundColor: "lightblue",
          width: "100%",
          height: "50px",
          position: "relative",
        }}
      >
        <InputGroup
          size="sm"
          style={{
            width: "80%",
            position: "absolute",
            top: "20%",
          }}
          onChange={(e) => setAddToDo(e.target.value)}
        >
          <InputGroup.Text
            id="toDoInput"
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
          >
            To Do:
          </InputGroup.Text>
          <Form.Control
            aria-label="Small"
            aria-describedby="toDoInput"
            style={{ borderRadius: "5px" }}
          />
        </InputGroup>
        <Button
          variant="primary"
          style={{ position: "absolute", left: "81%", top: "15%" }}
          onClick={handleAdd}
        >
          Add
        </Button>
        <Button
          variant="primary"
          style={{ position: "absolute", left: "85%", top: "15%" }}
          onClick={handleLogout}
        >
          Logout
        </Button>
      </div>

      <div
        className="toDoList"
        style={{
          paddingLeft: "50px",
          paddingTop: "10px",
          display: "flex",
          flexWrap: "wrap",
        }}
      >
        {data &&
          data.map((row, i) => {
            return (
              <div key={i} style={{ width: "30%" }}>
                <Form>
                  <div style={{ marginBottom: "5px" }}>
                    <Form.Check
                      type="checkbox"
                      id="default-checkbox"
                      label={row.todo}
                    />
                  </div>
                </Form>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default ToDoList;
