import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { postRegisterAPI, postLoginAPI } from "./reducerAPI";
const initialState = {
  data: [],
  status_login: "idle",
  status_register: "idle",
  message: "",
};

export const postRegister = createAsyncThunk("/register", async (payload) => {
  const response = await postRegisterAPI(payload);
  return response.data.message;
});

export const postLogin = createAsyncThunk("/login", async (payload) => {
  const response = await postLoginAPI(payload);
  return response.data.data;
});

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    reset: () => ({
      status_login: "idle",
      status_register: "idle",
      message: "",
    }),
    logout: () => initialState,
  },
  extraReducers(builder) {
    builder
      .addCase(postRegister.pending, (state) => {
        state.status_register = "loading";
      })
      .addCase(postRegister.fulfilled, (state) => {
        state.status_register = "succeeded";
      })
      .addCase(postRegister.rejected, (state, action) => {
        state.status_register = "failed";
        state.message = action.payload;
      })
      .addCase(postLogin.pending, (state) => {
        state.status_login = "loading";
      })
      .addCase(postLogin.fulfilled, (state, action) => {
        state.status_login = "succeeded";
        state.data = action.payload;
      })
      .addCase(postLogin.rejected, (state) => {
        state.status_login = "failed";
        state.error = "Failed to fetch data!";
      });
  },
});

export const { reset, logout } = authSlice.actions;
export const data = (state) => state.auth;
export default authSlice.reducer;
