import axios from "axios";

export const postRegisterAPI = async (payload) => {
  const res = await axios.post("http://localhost:5000/auth/register", payload);
  return res;
};

export const postLoginAPI = async (payload) => {
  const res = await axios.post("http://localhost:5000/auth/login", payload);
  return res;
};
