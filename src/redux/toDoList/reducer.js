import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { addToDoListAPI, showToDoListAPI } from "./reducerAPI";
const initialState = {
  data: [],
  error: "",
  status_add: "idle",
  status_show: "idle",
  message: "",
  detail: [],
};

export const addToDoList = createAsyncThunk("/add", async (payload) => {
  const response = await addToDoListAPI(payload);
  return response.data.message;
});

export const showToDoList = createAsyncThunk("/show", async (id) => {
  const response = await showToDoListAPI(id);
  return response.data.data;
});

const toDoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    reset: () => ({
      error: "",
      status_add: "idle",
      status_show: "idle",
      message: "",
    }),
  },
  extraReducers(builder) {
    builder
      .addCase(addToDoList.pending, (state) => {
        state.status_add = "loading";
      })
      .addCase(addToDoList.fulfilled, (state, action) => {
        state.status_add = "succeeded";
        state.message = action.payload;
      })
      .addCase(addToDoList.rejected, (state) => {
        state.status_add = "failed";
        state.error = "Failed to fetch data!";
      })
      .addCase(showToDoList.pending, (state) => {
        state.status_show = "loading";
      })
      .addCase(showToDoList.fulfilled, (state, action) => {
        state.status_show = "succeeded";
        state.data = action.payload;
      })
      .addCase(showToDoList.rejected, (state) => {
        state.status_show = "failed";
        state.error = "Failed to fetch data!";
      });
  },
});

export const { reset } = toDoSlice.actions;
export const data = (state) => state.todo;
export default toDoSlice.reducer;
