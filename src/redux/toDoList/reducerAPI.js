import axios from "axios";

export const addToDoListAPI = async (payload) => {
  const res = await axios.post(
    `http://localhost:5000/todolist/${payload.id}`,
    payload
  );
  return res;
};

export const showToDoListAPI = async (id) => {
  const res = await axios.get(`http://localhost:5000/todolist/${id}`);
  return res;
};
